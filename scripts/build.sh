#!/usr/bin/env bash
set -e

basepath=$(cd `dirname $0`; pwd)

cd $basepath/../

pwd

if [ ! -d "build" ]; then
  mkdir build
fi

cd build

cmake ..

make

