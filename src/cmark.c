//#include <stdlib.h>
//#include <assert.h>
//#include <stdio.h>
//#include "registry.h"
//#include "node.h"
//#include "houdini.h"
//#include "cmark-gfm.h"
//#include "buffer.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "config.h"
#include "memory.h"
#include "cmark-gfm.h"
#include "node.h"
#include "cmark-gfm-extension_api.h"
#include "syntax_extension.h"
#include "parser.h"
#include "registry.h"

//#include "../extensions/cmark-gfm-core-extensions.h"
#include "cmark-gfm-core-extensions.h"



//#include <stdlib.h>
//#include <stdio.h>
//#include <string.h>
//#include <errno.h>
//#include "config.h"
//#include "memory.h"
//#include "cmark-gfm.h"
//#include "node.h"
//#include "cmark-gfm-extension_api.h"
//#include "syntax_extension.h"
//#include "parser.h"
//#include "registry.h"
//#include "./cmark-gfm-core-extensions.h"

cmark_node_type CMARK_NODE_LAST_BLOCK = CMARK_NODE_FOOTNOTE_DEFINITION;
cmark_node_type CMARK_NODE_LAST_INLINE = CMARK_NODE_FOOTNOTE_REFERENCE;

int cmark_version() { return CMARK_GFM_VERSION; }

const char *cmark_version_string() { return CMARK_GFM_VERSION_STRING; }

static void *xcalloc(size_t nmem, size_t size) {
  void *ptr = calloc(nmem, size);
  if (!ptr) {
    fprintf(stderr, "[cmark] calloc returned null pointer, aborting\n");
    abort();
  }
  return ptr;
}

static void *xrealloc(void *ptr, size_t size) {
  void *new_ptr = realloc(ptr, size);
  if (!new_ptr) {
    fprintf(stderr, "[cmark] realloc returned null pointer, aborting\n");
    abort();
  }
  return new_ptr;
}

static void xfree(void *ptr) {
  free(ptr);
}

cmark_mem CMARK_DEFAULT_MEM_ALLOCATOR = {xcalloc, xrealloc, xfree};

cmark_mem *cmark_get_default_mem_allocator() {
  return &CMARK_DEFAULT_MEM_ALLOCATOR;
}


char *cmark_markdown_to_html(const char *text, size_t len, int options) {
//  cmark_node *doc;
//  char *result;
//
//  doc = cmark_parse_document(text, len, options);
//
//  result = cmark_render_html(doc, options, NULL);
//  cmark_node_free(doc);
//
//  return result;



//  printf ("=========================\n");

//  char buffer[4096];
//  size_t bytes;


//  int options = CMARK_OPT_DEFAULT;


  cmark_parser *parser = NULL;

  cmark_node *document = NULL;

  cmark_gfm_core_extensions_ensure_registered();


  parser = cmark_parser_new(options);


{
    cmark_syntax_extension *syntax_extension = cmark_find_syntax_extension("table");
    cmark_parser_attach_syntax_extension(parser, syntax_extension);
}
{
    cmark_syntax_extension *syntax_extension = cmark_find_syntax_extension("tasklist");
    cmark_parser_attach_syntax_extension(parser, syntax_extension);
}
{
    cmark_syntax_extension *syntax_extension = cmark_find_syntax_extension("strikethrough");
    cmark_parser_attach_syntax_extension(parser, syntax_extension);
}

  // ==================================================================
//  FILE *fp = fopen("/Users/yxl/CLionProjects/cmark-gfm/md/test.md", "rb");
//  while ((bytes = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
//    cmark_parser_feed(parser, buffer, bytes);
//    if (bytes < sizeof(buffer)) {
//      break;
//    }
//  }
//  fclose(fp);
  // ==================================================================

//  static const char string_with_null[] = "# h1\n- [x] 已完成事项";
//  cmark_parser_feed(parser, string_with_null,  sizeof(string_with_null) - 1);

  cmark_parser_feed(parser, text,  len);



  document = cmark_parser_finish(parser);

//  int width = 0;
//  writer_format writer = FORMAT_HTML;

    cmark_mem *mem = cmark_get_default_mem_allocator();

    char* html = cmark_render_html_with_mem(document, options, parser->syntax_extensions, mem);

//  char* html = print_document(document, writer, options, width, parser);
//  printf("[%s]", html);


  if (parser)
    cmark_parser_free(parser);

  if (document)
    cmark_node_free(document);

  cmark_release_plugins();

//  printf ("%s",html);

//  free(html);

//  printf ("=========================\n");






  return html;





}
