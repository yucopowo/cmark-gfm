#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "config.h"
#include "memory.h"
#include "cmark-gfm.h"
#include "node.h"
#include "cmark-gfm-extension_api.h"
#include "syntax_extension.h"
#include "parser.h"
#include "registry.h"

#include "../extensions/cmark-gfm-core-extensions.h"

typedef enum {
  FORMAT_NONE,
  FORMAT_HTML,
  FORMAT_XML,
  FORMAT_MAN,
  FORMAT_COMMONMARK,
  FORMAT_PLAINTEXT,
  FORMAT_LATEX
} writer_format;

void print_usage() {
  printf("Usage:   cmark-gfm [FILE*]\n");
  printf("Options:\n");
  printf("  --to, -t FORMAT   Specify output format (html, xml, man, "
         "commonmark, plaintext, latex)\n");
  printf("  --width WIDTH     Specify wrap width (default 0 = nowrap)\n");
  printf("  --sourcepos       Include source position attribute\n");
  printf("  --hardbreaks      Treat newlines as hard line breaks\n");
  printf("  --nobreaks        Render soft line breaks as spaces\n");
  printf("  --unsafe          Allow raw HTML and dangerous URLs\n");
  printf("  --smart           Use smart punctuation\n");
  printf("  --validate-utf8   Replace UTF-8 invalid sequences with U+FFFD\n");
  printf("  --github-pre-lang Use GitHub-style <pre lang> for code blocks\n");
  printf("  --extension, -e EXTENSION_NAME  Specify an extension name to use\n");
  printf("  --list-extensions               List available extensions and quit\n");
  printf("  --strikethrough-double-tilde    Only parse strikethrough (if enabled)\n");
  printf("                                  with two tildes\n");
  printf("  --table-prefer-style-attributes Use style attributes to align table cells\n"
         "                                  instead of align attributes.\n");
  printf("  --full-info-string              Include remainder of code block info\n"
         "                                  string in a separate attribute.\n");
  printf("  --help, -h       Print usage information\n");
  printf("  --version        Print version\n");
}

static bool print_document(cmark_node *document, writer_format writer,
                           int options, int width, cmark_parser *parser) {
  char *result;

  cmark_mem *mem = cmark_get_default_mem_allocator();

  switch (writer) {
  case FORMAT_HTML:
    result = cmark_render_html_with_mem(document, options, parser->syntax_extensions, mem);
    break;
  case FORMAT_XML:
    result = cmark_render_xml_with_mem(document, options, mem);
    break;
  case FORMAT_MAN:
    result = cmark_render_man_with_mem(document, options, width, mem);
    break;
  case FORMAT_COMMONMARK:
    result = cmark_render_commonmark_with_mem(document, options, width, mem);
    break;
  case FORMAT_PLAINTEXT:
    result = cmark_render_plaintext_with_mem(document, options, width, mem);
    break;
  case FORMAT_LATEX:
    result = cmark_render_latex_with_mem(document, options, width, mem);
    break;
  default:
    fprintf(stderr, "Unknown format %d\n", writer);
    return false;
  }
  printf("%s", result);
  mem->free(result);

  return true;
}

static void print_extensions(void) {
  cmark_llist *syntax_extensions;
  cmark_llist *tmp;

  printf ("Available extensions:\nfootnotes\n");

  cmark_mem *mem = cmark_get_default_mem_allocator();
  syntax_extensions = cmark_list_syntax_extensions(mem);
  for (tmp = syntax_extensions; tmp; tmp=tmp->next) {
    cmark_syntax_extension *ext = (cmark_syntax_extension *) tmp->data;
    printf("%s\n", ext->name);
  }

  cmark_llist_free(mem, syntax_extensions);
}

static void test0(void) {

  printf ("=========================\n");

  static const char string_with_null[] = "# h1\n- [x] 已完成事项";
  char *html = cmark_markdown_to_html(
          string_with_null,
          sizeof(string_with_null) - 1,
          CMARK_OPT_DEFAULT);

  printf ("%s",html);

  free(html);

  printf ("=========================\n");

}

static void test1(void) {

  printf ("=========================\n");

  char buffer[4096];
  size_t bytes;


  int options = CMARK_OPT_DEFAULT;


  cmark_parser *parser = NULL;

  cmark_node *document = NULL;

  cmark_gfm_core_extensions_ensure_registered();


  parser = cmark_parser_new(options);


  cmark_syntax_extension *syntax_extension = cmark_find_syntax_extension("table");
  cmark_parser_attach_syntax_extension(parser, syntax_extension);


  // ==================================================================
//  FILE *fp = fopen("/Users/yxl/CLionProjects/cmark-gfm/md/test.md", "rb");
//  while ((bytes = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
//    cmark_parser_feed(parser, buffer, bytes);
//    if (bytes < sizeof(buffer)) {
//      break;
//    }
//  }
//  fclose(fp);
  // ==================================================================

  static const char string_with_null[] = "# h1\n- [x] 已完成事项";
  cmark_parser_feed(parser, string_with_null,  sizeof(string_with_null) - 1);




  document = cmark_parser_finish(parser);

  int width = 0;
  writer_format writer = FORMAT_HTML;


  print_document(document, writer, options, width, parser);


  if (parser)
    cmark_parser_free(parser);

  if (document)
    cmark_node_free(document);

  cmark_release_plugins();

//  printf ("%s",html);

//  free(html);

  printf ("=========================\n");

}

int main(int argc, char *argv[]) {

  int res = 0;

  test0();

  return res;
}

